﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace OrderCloud.AzureStorage.Tests
{
	public class CosmosDbTests
	{
		private readonly CosmosDbService _cosmos =
			new CosmosDbService("testsenv", "localdev", "testcollection", _cosmosUrl, _cosmosDevKey);
		private static readonly string _cosmosUrl = "https://localhost:8081/";
		private static readonly string _cosmosDevKey = "C2y6yDjf5/R+ob0N8A7Cgv30VRDJIWEHLM+4QDU5DE2nQ9nDuVTqobD4b8mGGyPMbIZnqyMsEcaGQy67XIw/Jw==";//static key for emulator

		public class Testmodel
		{
			[CosmosDbIndexField]
			public string prop1 { get; set; }
			[CosmosDbIndexField]
			public string prop2 { get; set; }
			public string alwaysnull { get; set; }
		}

		public class Testmodel2
		{
			[CosmosDbIndexField]
			public string prop1 { get; set; }
			[CosmosDbIndexField]
			public string prop2 { get; set; }
		}


		[Fact]
		public async Task CanSearch()
		{
			await _cosmos.DestoryAndRestoreDocDbForDevOnlyAsync();
			await _cosmos.InitializeDocDb();

			async Task Insert(string p1, string p2)
			{
				await _cosmos.InsertDocAsync(new CosmosDoc<Testmodel> { Document = new Testmodel { prop1 = p1, prop2 = p2 } });
			}

			var tasks = new List<Task>();
			for (var i = 0; i <= 74; i++)
				tasks.Add(_cosmos.InsertDocAsync(new CosmosDoc<Testmodel> { Document = new Testmodel { prop1 = $"prop1 count{i.ToString()}", prop2 = $"Prop2 count{i.ToString()}" } }));

			for (var i = 0; i <= 74; i++)
				tasks.Add(_cosmos.InsertDocAsync(new CosmosDoc<Testmodel2> { Document = new Testmodel2 { prop1 = $"prop1 count{i.ToString()}", prop2 = $"prop2 count{i.ToString()}" } }));


			await Task.WhenAll(tasks.ToList());

			var l = await _cosmos.ListItemsAsync<Testmodel>();
			Assert.Equal(75, l.List.Count);
			Assert.Null(l.ContinuationToken);

			l = await _cosmos.ListItemsAsync<Testmodel>(maxItemCount: 2);
			Assert.Equal(2, l.List.Count);
			Assert.NotNull(l.ContinuationToken);

			l = await _cosmos.ListItemsAsync<Testmodel>(maxItemCount: 50, search: "count74");
			Assert.Single(l.List);
			Assert.Null(l.ContinuationToken);

			Expression<Func<CosmosDoc<Testmodel>, bool>> p = idoc => idoc.Document.prop1 == "prop1 count50";
			l = await _cosmos.ListItemsAsync<Testmodel>(maxItemCount: 50, predicate: p);
			Assert.Single(l.List);
			Assert.Null(l.ContinuationToken);

			l = await _cosmos.ListItemsAsync<Testmodel>(maxItemCount: 50, search: "pRop2 couNt50");
			Assert.Single(l.List);
			Assert.Null(l.ContinuationToken);
		}


		[Fact]
		public async Task ListHasDocType()
		{
			await _cosmos.DestoryAndRestoreDocDbForDevOnlyAsync();
			await _cosmos.InitializeDocDb();
			var doc = new CosmosDoc<Testmodel> {Document = new Testmodel {prop1 = "prop", prop2 = "2"}};
			var insert = await _cosmos.InsertDocAsync(doc);
			var get = await _cosmos.GetDocAsnyc<Testmodel>(insert.id);
			Assert.Equal("prop", get.Document.prop1);
			Assert.NotNull(get.DocType);
		}
	}
}

