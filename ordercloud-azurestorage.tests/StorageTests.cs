﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Blob;
using OrderCloud.AzureStorage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using Xunit;


namespace OrderCloud.AzureStorage.Tests
{
	public class PlatformStorageTests
	{
		private readonly TableService _tableService;
		private readonly BlobService _blobService;
		private readonly CosmosToBlobCopyService _copyService;
		private readonly CosmosDbService _dbcollection1;
		private readonly CosmosDbService _dbcollection2;
		private readonly string _storageConnection = "UseDevelopmentStorage=true;";
		private readonly string _cosmosUrl = "https://localhost:8081/";
		private readonly string _cosmosDevKey = "C2y6yDjf5/R+ob0N8A7Cgv30VRDJIWEHLM+4QDU5DE2nQ9nDuVTqobD4b8mGGyPMbIZnqyMsEcaGQy67XIw/Jw==";//static key for emulator

		public PlatformStorageTests()
		{
			_tableService = new TableService(_storageConnection);
			_blobService = new BlobService(_storageConnection); //container.GetInstance<BlobStorageService>();
			_copyService = new CosmosToBlobCopyService(_cosmosDevKey, _cosmosUrl, _storageConnection); // container.GetInstance<CosmosToBlobCopyService>();
			_dbcollection1 = new CosmosDbService("dev", "localdev", "collection1", _cosmosUrl, _cosmosDevKey);
			_dbcollection2 = new CosmosDbService("dev", "localdev", "collection2", _cosmosUrl, _cosmosDevKey);
		}

		public class Person : TableEntity
		{
			public string what { get; set; }
			public string thex { get; set; }
		}
		[Fact]
		public async Task SetAnonAccessOnBlobContainer()
		{
			await _blobService.DestroyAllBlogStorageForDevTestOnly();
			await _blobService.SetAnonAccessOnContainerAsync("webstorage", BlobContainerPublicAccessType.Container);
			var containerRef = _blobService.BlobClient.GetContainerReference("webstorage");
			Assert.Equal(BlobContainerPublicAccessType.Container, containerRef.GetPermissions().PublicAccess);
		}
		[Fact]
		public async Task InsertObject()
		{
			await _tableService.DestoryTablesForDevOnlyAsync();
			var p = new Person { what = "huh", thex = "f", PartitionKey = "jeff", RowKey = "ilse" };
			await _tableService.InsertOrReplaceAsync(p);
			var o = await _tableService.GetAsync<Person>("jeff", "ilse");
			Assert.Equal(o.what, p.what);
			await _tableService.DeleteAsync<Person>("jeff", "ilse");
			await Assert.ThrowsAsync<Exceptions.NotFoundException>(() => _tableService.GetAsync<Person>("jeff", "ilse"));
		}

		[Fact]
		public async Task TestBlobService()
		{
			await _blobService.DestroyAllBlogStorageForDevTestOnly();
			var st = new MemoryStream();
			var bytes = Encoding.ASCII.GetBytes("some file data");
			st.Write(bytes, 0, bytes.Length);
			st.Seek(0, SeekOrigin.Begin);
			await _blobService.WriteBlockBlobFromStreamAsync("testcontainer", "ablob.txt", st);
			await _blobService.WriteBlockBlobFromStringBodyAsync("textcontainer", "atextfile.txt", "a bunch of text from a file");
			//assert that file exists
		}

		[Fact]
		public async Task ListAllTableRowsByPartitionKey()
		{
			await _tableService.DestoryTablesForDevOnlyAsync();
			await _tableService.InsertOrReplaceAsync(new Person { what = "1", thex = "f", PartitionKey = "groupa", RowKey = "smith" });
			await _tableService.InsertOrReplaceAsync(new Person { what = "2", thex = "f", PartitionKey = "groupa", RowKey = "johnson" });
			await _tableService.InsertOrReplaceAsync(new Person { what = "3", thex = "f", PartitionKey = "groupa", RowKey = "ilse" });
			await _tableService.InsertOrReplaceAsync(new Person { what = "4", thex = "f", PartitionKey = "groupb", RowKey = "ilse" });
			await _tableService.InsertOrReplaceAsync(new Person { what = "5", thex = "f", PartitionKey = "groupb", RowKey = "johnson" });

			var all = _tableService.ListAll<Person>().ToList();
			Assert.Equal(5, all.Count);
			var list = _tableService.ListAllbyPartitionKey<Person>("groupa");
			Assert.Collection(list, i=> Assert.Contains("3", i.what), i => Assert.Contains("2", i.what), i => Assert.Contains("1", i.what));
			await _tableService.DeleteAllByPartitionKeyAsync<Person>("groupa");
			list = _tableService.ListAllbyPartitionKey<Person>("groupa");
			Assert.Empty(list);
			list = _tableService.ListAllbyPartitionKey<Person>("groupb");
			Assert.Equal(2, list.Count);
		}

		[Fact]
		public async Task TestCosmosBackup()
		{
			await _blobService.DestroyAllBlogStorageForDevTestOnly();
			await _dbcollection1.DestoryAndRestoreDocDbForDevOnlyAsync();
			await _dbcollection2.InitializeDocDb();
			var doc1 = await InsertAsync("prop1db1", 1, _dbcollection1);
			await InsertAsync("prop2db1", 2, _dbcollection1);
			await InsertAsync("prop1db2", 3, _dbcollection2);
			await InsertAsync("prop2db2", 4, _dbcollection2);
			await _copyService.CopyItAllAsync("localdev", Console.Out);
			var backedupfileText = await _blobService.ReadTextFileBlobAsync($"cosmosbackup-localdev-collection1", doc1.id + ".json");
			var doc = JsonConvert.DeserializeObject<CosmosDoc<TestDbDoc>>(backedupfileText);
			Assert.Equal("prop1db1", doc.Document.aprop);
			//assert that backup files exist
		}

		public class TestDbDoc
		{
			public string aprop { get; set; }
			public int anumber { get; set; }
		}

		private async Task<CosmosDoc<TestDbDoc>> InsertAsync(
			string aprop,
			int anumber,
			CosmosDbService db)
		{
			var doc = new CosmosDoc<TestDbDoc>
			{
				Document = new TestDbDoc
				{
					aprop = aprop,
					anumber = anumber
				}
			};
			return await db.InsertDocAsync(doc);
		}
	}
}
