﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Xunit;
using Microsoft.Extensions.Configuration;
using OrderCloud.AzureStorage;

namespace OrderCloud.AzureStorage.Tests
{
	public class ConfigTests
	{
		private static readonly string _storageConn = "UseDevelopmentStorage=true;";
		private readonly TableService _table = new TableService(_storageConn);

		public class ConnectionStrings
		{
			public string ConnName { get; set; }
			public string ConnName2 { get; set; }
		}

		public class MyAppSettings
		{
			public string appSetting1 { get; set; }
			public string appSetting2 { get; set; }
			public int appSetting3 { get; set; }
			public ConnectionStrings ConnectionStrings { get; set; }

		}
		private async Task buildConfig(string prop1, string prop2, int prop3, string conn1, string conn2, string env, string appname)
		{
			var props = new DynamicObjectTableEntity { PartitionKey = env, RowKey = "settings" };
			props.properties["appSetting1"] = new Microsoft.WindowsAzure.Storage.Table.EntityProperty(prop1);
			props.properties["appSetting2"] = new Microsoft.WindowsAzure.Storage.Table.EntityProperty(prop2);
			props.properties["appSetting3"] = new Microsoft.WindowsAzure.Storage.Table.EntityProperty(prop3);
			props.properties["ConnectionStrings11ConnName"] = new Microsoft.WindowsAzure.Storage.Table.EntityProperty(conn1);
			props.properties["ConnectionStrings11ConnName2"] = new Microsoft.WindowsAzure.Storage.Table.EntityProperty(conn2);
			await _table.InsertOrReplaceAsync(props, "appsettings" + appname);
		}
		private async Task<MyAppSettings> GetConfig(string env, string appname, string DEVOPS_STORAGE)
		{
			Environment.SetEnvironmentVariable("DEVOPS_STORAGE", DEVOPS_STORAGE);
			Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", env);
			Environment.SetEnvironmentVariable("DEVOPS_APPNAME", appname);
			var builder = new ConfigurationBuilder();
			builder.AddAzureTableConfiguration();
			var root = builder.Build();
			return root.Get<MyAppSettings>();
		}

		private async Task configure2(string appname)
		{
			await _table.DestoryTablesForDevOnlyAsync();
			await buildConfig("what", "the", 15, "devconn1", "devconn2", "dev", appname);
			await buildConfig("other", "value", 20, "prodconn1", "prodconn2", "prod", appname);
		}

		[Fact]
		public async Task CanConfigure()
		{
			var appname = "myappname";
			await configure2(appname);
			var dev = await GetConfig("dev", appname, _storageConn);
			var prod = await GetConfig("prod", appname, _storageConn);

			Assert.Equal("what", dev.appSetting1);
			Assert.Equal("the", dev.appSetting2);
			Assert.Equal(15, dev.appSetting3);
			Assert.Equal("devconn1", dev.ConnectionStrings.ConnName);
			Assert.Equal("devconn2", dev.ConnectionStrings.ConnName2);
			Assert.Equal("other", prod.appSetting1);
			Assert.Equal("value", prod.appSetting2);
			Assert.Equal(20, prod.appSetting3);
			Assert.Equal("prodconn1", prod.ConnectionStrings.ConnName);
			Assert.Equal("prodconn2", prod.ConnectionStrings.ConnName2);

		}


		[Fact]
		public async Task MisConfigThrows()
		{
			var appname = "wontworkwithoutconn";
			await configure2(appname);
			await GetConfig("dev", appname, _storageConn); //doesn't throw
			await Assert.ThrowsAsync<Exceptions.MissingEnvironmentVar>(()=> GetConfig("dev", appname, null));
			await Assert.ThrowsAsync<System.AggregateException>(()=> GetConfig("wrongevnname", appname, _storageConn));//TODO:this should really throw a not found
		}
	}
}
