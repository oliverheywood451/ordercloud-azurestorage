﻿using System.IO;

namespace OrderCloud.AzureStorage
{
	public interface IQueueService
	{
		void CleanupStorage(TextWriter logger);
		string DropFileByStream(string queuename, Stream stream, params string[] qparams);
		void DropMessage(string queuename, object message);
		Stream OpenFileStream(string fileName);
		void ReadQueuedFile(string fileName, out string url, out string body);
	}
}