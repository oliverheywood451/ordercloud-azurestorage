﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Documents.Client;
using Newtonsoft.Json;

namespace OrderCloud.AzureStorage
{
	public class CosmosToBlobCopyService
	{
		private readonly string _cosmosKey, _cosmosUrl, _storageConnectionString;
		private readonly BlobService _blobService;
		public CosmosToBlobCopyService(string cosmosKey, string cosmosUrl, string storageConnectionString)
		{
			_cosmosKey = cosmosKey;
			_cosmosUrl = cosmosUrl;
			_storageConnectionString = storageConnectionString;
			_docClient = new DocumentClient(new Uri(_cosmosUrl), _cosmosKey);
			_blobService = new BlobService(storageConnectionString);
		}

		private string _databasename;
		private DocumentClient _docClient;
		public async Task CopyItAllAsync(string database, TextWriter log)
		{
			_databasename = database;
			//var db = await _docClient.ReadDatabaseAsync(UriFactory.CreateDatabaseUri(database));
			var collections =await _docClient.ReadDocumentCollectionFeedAsync(UriFactory.CreateDatabaseUri(database));
			foreach(var c in collections)
			{
				log.WriteLine($"backing collection: {c.Id}");
				string continuation = string.Empty;
				int count = 0;
				do
				{
					var docs = await _docClient.ReadDocumentFeedAsync(c.DocumentsLink,
						new FeedOptions
						{
							MaxItemCount = 10,
							RequestContinuation = continuation
						});
					foreach(var doc in docs)
					{
						var id = (string) doc.Id;
						//var body = JsonConvert.SerializeObject(doc);
						var container = "cosmosbackup-" + database.ToLower() + "-" + c.Id.ToLower();
						await _blobService.WriteBlockBlobFromStringBodyAsync(container, id +".json", Convert.ToString(doc));
						
					};
					continuation = docs.ResponseContinuation;
				} while (!string.IsNullOrEmpty(continuation));
			}
		}
	}
}
