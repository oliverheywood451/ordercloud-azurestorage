﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
namespace OrderCloud.AzureStorage
{
	public interface IGhedisCache
	{
		Task CompareAndCacheAsync<T>(IEnumerable<GhedisItem<T>> runItemsOp, Func<GhedisItem<T>, Task> oneChanged);
		Action<string> WriteLog { get; set; }
	}
	public interface IGhedisClassFactory
	{
		Task<IGhedisCache> CreateCacheAsync(string uniqueName);
	}
	public class GhedisFileSystemClassFactory : IGhedisClassFactory
	{
		private readonly string _overrideFilePath;
		private readonly int _throttle;
		public GhedisFileSystemClassFactory()
		{
		}
		public GhedisFileSystemClassFactory(string overrideFilePath, int msThrottle)
		{
			_throttle = msThrottle;
			_overrideFilePath = overrideFilePath;
		}
		public async Task<IGhedisCache> CreateCacheAsync(string uniqueName)
		{
			var c = new GhedisFileSystemCache(uniqueName, _throttle, _overrideFilePath);//could override folder from app settings here
			await c.InitAsync();
			return c;
		}
	}
	public class GhedisMockClassFactory : IGhedisClassFactory
	{
		public Task<IGhedisCache> CreateCacheAsync(string uniqueName)
		{
			return Task.FromResult<IGhedisCache>(new GhedisMockCache());
		}
	}

	public class GhedisAzureTableClassFactory : IGhedisClassFactory
	{
		private readonly string _storageConnectionString;
		public GhedisAzureTableClassFactory(string storageConnectionString)
		{
			_storageConnectionString = storageConnectionString;
		}
		public Task<IGhedisCache> CreateCacheAsync(string uniqueName)
		{
			return Task.FromResult<IGhedisCache>(new GhedisAzureTableCache(_storageConnectionString, uniqueName));
		}
	}

	public class GhedisItem<T>
	{
		public GhedisItem() { }
		public string Key { get; set; }
		public T ObjectToCompare { get; set; }
	}

	public abstract class GhedisBase : IGhedisCache
	{
		//internal abstract List<string> LastCache{ get; }
		internal abstract Task<bool> ContainsInLastCacheAsync(string key, string hash);
		public abstract Action<string> WriteLog { get; set; }
		internal abstract Task WriteRowToNextCacheAsync(string key , string hash, bool changedInThisRun);
		public string UniqueCachename  { get; }
		public int ThrottleMS { get; }
		internal GhedisBase(string uniquename, int throttleMS = 0)
		{
			ThrottleMS = throttleMS;
			UniqueCachename = uniquename;
		}
		public async Task CompareAndCacheAsync<T>(IEnumerable<GhedisItem<T>> runItemsOp, Func<GhedisItem<T>, Task> oneChangedAsync)
		{
			var count = 0;
			var changed = 0;
			
			foreach (var o in runItemsOp)
			{
				if (count > 0 && count % 100 == 0) WriteLog($"so far {changed} changed of {count}");

				var serialized = JsonConvert.SerializeObject(o.ObjectToCompare);
				//var cacheRow = $"{o.Key}-{CreateMD5(serialized)}";
				var hash = CreateMD5(serialized);

				var contains = await ContainsInLastCacheAsync(o.Key, hash);
				if (!contains)
				{
					if (ThrottleMS > 0) await Task.Delay(ThrottleMS); //only throttle if the action is actually happening.
					//WriteLog?.Invoke($"drop {o.Key} to {UniqueCachename}");
					await oneChangedAsync(o);
					changed++;
				}
				count++;
				await WriteRowToNextCacheAsync(o.Key, hash, contains);
			}
			WriteLog?.Invoke($"changed {changed.ToString()} of {count.ToString()}");
		}

		internal static string CreateMD5(string input)
		{
			// Use input string to calculate MD5 hash
			using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
			{
				byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
				byte[] hashBytes = md5.ComputeHash(inputBytes);

				// Convert the byte array to hexadecimal string
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < hashBytes.Length; i++)
				{
					sb.Append(hashBytes[i].ToString("X2"));
				}
				return sb.ToString();
			}
		}
	}

	public class GhedisAzureTableCache : GhedisBase
	{
		private readonly TableService _table;
		private readonly string _uniqueName;

		public override Action<string> WriteLog { get; set; }

		public GhedisAzureTableCache(string storageconnectionstring, string uniquename, int throttleMS = 0) : base(uniquename, throttleMS)
		{
			_uniqueName = uniquename;
			_table = new AzureStorage.TableService(storageconnectionstring);
			WriteLog = s => { };
		}
		internal override async Task WriteRowToNextCacheAsync(string key, string hash, bool containedInThiRun)
		{
			if (!containedInThiRun) //other wise no change it's already there not needing an update
				await _table.InsertOrReplaceAsync(new GhedisCacheRow(_uniqueName, key, hash));
		}
		internal override async Task<bool> ContainsInLastCacheAsync(string key, string hash)
		{
			try
			{
				var r = await _table.GetAsync<GhedisCacheRow>(_uniqueName, key);
				return r.Hash == hash;
			}
			catch (Exceptions.NotFoundException)
			{
				return false;
			}
		}
		public class GhedisCacheRow : TableEntity
		{
			public GhedisCacheRow()
			{
			}
			public string Hash { get; set; }
			public GhedisCacheRow(string uniqueTypeName, string key, string hash)
			{
				this.PartitionKey = uniqueTypeName;
				this.RowKey = key;
				Hash = hash;
			}
		}
		
	}
	public class GhedisFileSystemCache : GhedisBase
	{
		private DirectoryInfo CacheDir;
	    private readonly string _overrideOperatingFolder;
		private readonly string _uniqueCacheName;
		private string _nextCachePath;
		private string _logPath;
		private List<string> _lastCache;

		internal GhedisFileSystemCache(string uniqueCacheName, int throttleMS, string overrideFolder = null ) : base(uniqueCacheName, throttleMS)
		{
			_uniqueCacheName = uniqueCacheName;
			_overrideOperatingFolder = overrideFolder;
			WriteLog = message =>
			{
				var m = $"{DateTime.Now.ToLongTimeString()} - {message}\r\n";
				Console.Write(m);
				File.AppendAllText(_logPath, m);
			};

		}
		internal async Task InitAsync()
		{
			var rootFolder = _overrideOperatingFolder ?? Directory.GetCurrentDirectory();
			CacheDir = Directory.CreateDirectory($"{rootFolder}/cachefiles/{_uniqueCacheName}");
			var logDir = Path.Combine(rootFolder, "logs", _uniqueCacheName);
			Directory.CreateDirectory(logDir);
			_logPath = Path.Combine(logDir, Path.GetRandomFileName() + ".log");
			int max = 0;
			CacheDir.GetFiles().ToList().ForEach(f =>
			{
				var filenumber = Convert.ToInt32(f.Name);
				max = Math.Max(filenumber, max);
			});
			var nextRunID = max + 1;
			var lastCachePath = $"{CacheDir.FullName}/{max.ToString()}";
			_nextCachePath = $"{CacheDir.FullName}/{nextRunID.ToString()}";
			if (File.Exists(_nextCachePath))
				File.Delete(_nextCachePath);
			_lastCache = File.Exists(lastCachePath) ? File.ReadAllLines(lastCachePath).ToList() : new List<string>();
			CacheDir.GetFiles().Where(f => Convert.ToInt32(f.Name) < max - 4).ToList().ForEach(f => f.Delete());//only keep 5
		}

		internal override Task<bool> ContainsInLastCacheAsync(string key, string hash) => Task.FromResult(_lastCache.Contains(key + "-" + hash));

		public override Action<string> WriteLog { get; set; }

		internal override Task WriteRowToNextCacheAsync(string key, string hash, bool containedInLast)
		{
			System.IO.File.AppendAllText(_nextCachePath, $"{key}-{hash}\r\n");
			return Task.FromResult(0);
		}
	}
	public class GhedisMockCache : GhedisBase
	{
		internal GhedisMockCache() : base("mock")
		{
			WriteLog = Console.WriteLine;
		}
		public override Action<string> WriteLog { get; set; }
		internal override Task<bool> ContainsInLastCacheAsync(string key, string has) => Task.FromResult(false);
		internal override Task WriteRowToNextCacheAsync(string key, string hash, bool contained) => Task.FromResult(0);
	}
}
