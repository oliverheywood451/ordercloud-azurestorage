﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;

namespace OrderCloud.AzureStorage
{
	public class BlobService
	{
		private readonly CloudStorageAccount _storageAccount;
		private readonly CloudBlobClient _blobClient;
		private readonly string _connection;
		public async Task DestroyAllBlogStorageForDevTestOnly()
		{
			if (_connection != "UseDevelopmentStorage=true;")
				throw new Exception("this will delete all your datas. only for dev/test purposes");

			_blobClient.ListContainers().ToList().ForEach(async c => await c.DeleteAsync());
		}
		public CloudBlobClient BlobClient => _blobClient;

		public BlobService(string connectionString)
		{
			_connection = connectionString;
			_storageAccount = CloudStorageAccount.Parse(connectionString);
			_blobClient = _storageAccount.CreateCloudBlobClient();
			
		}
		public async Task SetAnonAccessOnContainerAsync(string storageContainerName, BlobContainerPublicAccessType accessType = BlobContainerPublicAccessType.Blob)
		{
			var containerRef = _blobClient.GetContainerReference(storageContainerName);
			containerRef.CreateIfNotExists();
			BlobContainerPermissions permissions = containerRef.GetPermissions();
			permissions.PublicAccess = accessType;
			await containerRef.SetPermissionsAsync(permissions);
		}
		public async Task<Uri> WriteBlockBlobFromStreamAsync(string storageContainerName, string blobName, Stream fileStream)
		{
			var containerRef = _blobClient.GetContainerReference(storageContainerName);
			containerRef.CreateIfNotExists();
			var blob = containerRef.GetBlockBlobReference(blobName);
			await blob.UploadFromStreamAsync(fileStream);
			return blob.Uri;
		}
		public async Task<Uri> WriteBlockBlobFromStringBodyAsync(string storageContainerName, string blobName, string textOfFile)
		{
			var containerRef = _blobClient.GetContainerReference(storageContainerName);
			containerRef.CreateIfNotExists();
			var blob = containerRef.GetBlockBlobReference(blobName);
			await blob.UploadTextAsync(textOfFile);
			return blob.Uri;
		}

		public async Task<string> ReadTextFileBlobAsync(string storageContainerName, string blobName)
		{
			var container = _blobClient.GetContainerReference(storageContainerName);
			var blob = container.GetBlockBlobReference(blobName);
			//url = blob.Uri.ToString();
			var body = await blob.DownloadTextAsync();
			return body;
		}
	}
}
