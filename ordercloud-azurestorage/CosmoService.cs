﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Newtonsoft.Json;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Azure.Documents;

namespace OrderCloud.AzureStorage
{
	public class CosmosDbIndexField : Attribute
	{

	}
	public class CosmosDocListPage<T>
	{
		public string ContinuationToken { get; set; }
		public List<CosmosDoc<T>> List { get; set; }
	}
	public class CosmosDoc<T>
	{
		public string Env { get; set; }
		public string DocType { get; set; }
		public T Document { get; set; }
		public string id { get; set; }
		public string[] IndexedWords { get; set; }
	}
	public class CosmosDbService
	{
		private readonly DocumentClient _docClient;
		private readonly Uri _collectionUri;

		public DocumentClient DocClient => _docClient;
		public Uri CollectionUri => _collectionUri;

		private readonly string _env, _dbname, _collectionname;

		public CosmosDbService(string env, string databasename, string collectionname, string cosmoUrl, string cosmoKey)
		{
			_env = env;
			_dbname = databasename;
			_collectionname = collectionname;
			_collectionUri = UriFactory.CreateDocumentCollectionUri(_dbname, _collectionname);
			_docClient = new DocumentClient(new Uri(cosmoUrl), cosmoKey);
		}

		public async Task InitializeDocDb(int throughput = 400, UniqueKeyPolicy policy = null)
		{
			try
			{
				await _docClient.ReadDatabaseAsync(UriFactory.CreateDatabaseUri(_dbname));
			}
			catch (DocumentClientException e) when (e.StatusCode == System.Net.HttpStatusCode.NotFound)
			{
				await _docClient.CreateDatabaseAsync(new Database { Id = _dbname });
			}

			try
			{
				await _docClient.ReadDocumentCollectionAsync(UriFactory.CreateDocumentCollectionUri(_dbname, _collectionname));
			}
			catch (DocumentClientException e) when (e.StatusCode == System.Net.HttpStatusCode.NotFound)
			{
				//https://docs.microsoft.com/en-us/azure/cosmos-db/unique-keys
				DocumentCollection myCollection = new DocumentCollection();
				myCollection.Id = _collectionname;
				if (policy != null)
				{
					//myCollection.PartitionKey.Paths.Add("/pk");
					myCollection.UniqueKeyPolicy = policy;
				}
				await _docClient.CreateDocumentCollectionAsync(
					UriFactory.CreateDatabaseUri(_dbname),
					myCollection,
					new RequestOptions { OfferThroughput = throughput });
			}
		}

		public async Task DestoryAndRestoreDocDbForDevOnlyAsync()
		{
			if (!(_docClient.ServiceEndpoint.Host == "localhost" || _docClient.ServiceEndpoint.Host != "127.0.0.1"))
				throw new Exception("only devs can destroy local databases!!!!");

			await _docClient.DeleteDatabaseAsync(UriFactory.CreateDatabaseUri(_dbname));
			await InitializeDocDb();
		}
		//https://azure.microsoft.com/en-us/blog/searching-for-text-with-documentdb/
		private string[] WordsWordsWords<T>(CosmosDoc<T> doc)
		{
			HashSet<string> set = new HashSet<string>();
			var classHasIndexAttr = typeof(T).CustomAttributes.Any(a => a.AttributeType == typeof(CosmosDbIndexField));

			doc.Document.GetType().GetProperties()
				.Where(prop => classHasIndexAttr || prop.CustomAttributes.Any(a => a.AttributeType == typeof(CosmosDbIndexField)))
				.ToList()
				.ForEach(prop =>
				{
					prop.GetValue(doc.Document)?.ToString().Split(' ').ToList().ForEach(s => set.Add(s.ToLower()));
				});

			return set.ToArray();
		}

		public async Task<CosmosDoc<T>> InsertDocAsync<T>(CosmosDoc<T> doc)
		{
			var t = typeof(T).Name;
			var rr = await _docClient.CreateDocumentAsync(
				_collectionUri,
				new CosmosDoc<T> { Env = _env, DocType = t, Document = doc.Document, IndexedWords = WordsWordsWords(doc) });
			return JsonConvert.DeserializeObject<CosmosDoc<T>>(rr.Resource.ToString());
		}

		public async Task<CosmosDoc<T>> ReplaceDocAsync<T>(CosmosDoc<T> doc)
		{
			if (doc.id == null)
				throw new Exceptions.NotFoundException();

			var t = typeof(T).Name;
			var get = await GetDocAsnyc<T>(doc.id);
			Uri docuri = UriFactory.CreateDocumentUri(_dbname, _collectionname, get.id);
			var rr = await _docClient.ReplaceDocumentAsync(docuri, new CosmosDoc<T> { Env = _env, DocType = t, Document = doc.Document, id = get.id, IndexedWords = WordsWordsWords<T>(doc) });
			return JsonConvert.DeserializeObject<CosmosDoc<T>>(rr.Resource.ToString());
		}

		public IQueryable<CosmosDoc<T>> GetQueryWithFilters<T>(Expression<Func<CosmosDoc<T>, bool>> predicate, string continuationToken = null, string search = null, int maxItemCount = 100)
		{
			var q = _docClient.CreateDocumentQuery<CosmosDoc<T>>(
					_collectionUri,
					new FeedOptions
					{
						MaxItemCount = maxItemCount,
						RequestContinuation = continuationToken == null
							? null
							: ASCIIEncoding.UTF8.GetString(Base64UrlTextEncoder.Decode(continuationToken))
					})
				.Where(cd => cd.Env == _env && cd.DocType.ToString() == typeof(T).Name);


			search?.Split(' ').ToList().ForEach(s =>
			{
				q = q.Where(cd => cd.IndexedWords.Contains(s.ToLower()));
			});


			if (predicate != null)
				q = q.Where(predicate);

			return q;
		}

		public async Task<CosmosDocListPage<T>> ListItemsAsync<T>(Expression<Func<CosmosDoc<T>, bool>> predicate = null,string continuationToken = null, string search = null, int maxItemCount = 100)
		{
			var query = GetQueryWithFilters<T>(predicate, continuationToken, search, maxItemCount).AsDocumentQuery();

			var result = new CosmosDocListPage<T>();

			List<CosmosDoc<T>> results = new List<CosmosDoc<T>>();
			if (query.HasMoreResults)
			{
				var qresult = await query.ExecuteNextAsync<CosmosDoc<T>>();
				result.ContinuationToken = qresult.ResponseContinuation == null ? null : Base64UrlTextEncoder.Encode(ASCIIEncoding.UTF8.GetBytes(qresult.ResponseContinuation)); ;
				result.List = qresult.ToList<CosmosDoc<T>>();
			}
			return result;
		}
		public async Task<CosmosDoc<T>> GetDocAsnyc<T>(string id)
		{
			var l = await this.ListItemsAsync<T>(x => x.id == id);//keeps all the env/docttype fileters in one place. Could also just get by id and verify type and env
			if (l.List.Count == 0)
				throw new Exceptions.NotFoundException();
			return l.List.First();
		}
		public async Task DeleteItemAsnyc<T>(string id)
		{
			var get = await this.GetDocAsnyc<T>(id);//to double check it's the right type/env
			Uri docuri = UriFactory.CreateDocumentUri(_dbname, _collectionname, get.id);
			await _docClient.DeleteDocumentAsync(docuri);
		}
	}
}
